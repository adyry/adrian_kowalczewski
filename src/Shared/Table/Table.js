import React from 'react';
import '../../typedef.js'
import Spinner from '../Spinner.js';

export const Table = (props) => <table id={props.id} className="table table-bordered table-hover">
  <thead className="thead-light">
    <tr>
      {props.config.columns.map((v, i) => <th scope="col" key={i}>{v.name}</th>)}
      {props.config.showActions ? <th scope="col">Actions</th> : null}
    </tr>
  </thead>
  <tbody>
    {props.data
      ? (props.data.length
        ? props.data.map(row => <React.Fragment key={row.id}>
          <tr>
            {props.config.columns.map((property, i) => <td key={i}>{row[property.value]}</td>)}
            {props.config.showActions
              ? <td>
                <div
                  className="btn-group btn-group-sm"
                  role="group"
                  aria-label="Actions"
                >
                  <button type="button" className="btn btn-secondary">
                    <i className="fa fa-pencil" aria-hidden="true" /> Edit
                </button>
                  <button type="button" className="btn btn-danger">
                    <i className="fa fa-trash-o" aria-hidden="true" /> Remove
                </button>
                </div>
              </td>
              : null}
          </tr>
        </React.Fragment>)
        : <tr>
          <th colSpan={6}>
            <div className="text-center">
              No Results Found
            </div>
          </th>
        </tr>)
      : <tr>
        <th colSpan={6}>
          <Spinner />
        </th>
      </tr>}

  </tbody>
</table>
