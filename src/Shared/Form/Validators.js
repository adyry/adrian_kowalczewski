
export const errors = {
  required: 'This field is required.'
}

export const nonEmptySetter = (v, setter) => {
  if (v === '') {
    setter({ value: v, error: errors.required });
  } else {
    setter({ value: v, error: '' });
  };
};

export const emptyCheck = (v, setter) => {
  if (v === '') {
    setter((prev) => ({ ...prev, error: errors.required }));
    return true;
  };
};