import React from 'react';
import { useSpecies } from '../../Helpers/hooks';
import './styles.css';
import RadioOption from '../../Shared/Form/RadioOption';
import { nonEmptySetter, emptyCheck } from '../../Shared/Form/Validators';
import Spinner from '../../Shared/Spinner';
import { fetchUtil } from '../../Helpers/utils';

export const AddCharacter = (props) => {
  const emptyValue = { value: '', error: '' };
  const [name, setName] = React.useState(emptyValue);
  const [species, setSpecies] = React.useState(emptyValue);
  const [gender, setGender] = React.useState(emptyValue);
  const [homeworld, setHomeworld] = React.useState(emptyValue);
  const requiredFields = [name, species, gender];
  const requiredSetters = [setName, setSpecies, setGender];

  const nameRef = React.useRef(null);
  const speciesRef = React.useRef(null);
  const genderRef = React.useRef(null);
  const refs = [nameRef, speciesRef, genderRef];

  const [submitDisabled, setSubmitDisabled] = React.useState(false);

  const speciesList = useSpecies();

  const handleSubmit = (e) => {
    e.preventDefault();
    const hasErrors = requiredFields
      .map((v, i) => emptyCheck(v.value, requiredSetters[i]))
      .find((checkOutput, i) => {
        if (checkOutput) {
          refs[i].current.focus();
        }
        return checkOutput;
      });
    if (!hasErrors) {
      setSubmitDisabled(true);
      const body = {
        gender: gender.value,
        homeworld: homeworld.value,
        name: name.value,
        species: species.value
      };
      fetchUtil('/characters', 'POST', JSON.stringify(body)).then((response) => {
        if (response.id) {
          props.history.push('/');
        } else {
          setSubmitDisabled(false);
        };
      });
    };
  };

  return <div>
    {speciesList.length > 0
      ? <form className="border border-light p-5" onSubmit={handleSubmit} id="AddForm">
        <p className="h4 text-center">Add Character</p>

        <label htmlFor="firstName" className="required">Name</label>
        <input type="text" id="firstName" className={`form-control mb-2 ${name.error ? 'is-invalid' : ''}`}
          value={name.value}
          onChange={e => nonEmptySetter(e.target.value, setName)} ref={nameRef} />
        <div className="invalid-feedback" style={{ display: `${name.error ? 'block' : 'none'}` }}>
          {name.error}
        </div>

        <label htmlFor="SpeciesSelect" className="required">Species</label>
        <select className={`browser-default custom-select mb-2 ${species.error ? 'is-invalid' : ''}`} id="SpeciesSelect"
          value={species.value}
          onChange={e => nonEmptySetter(e.target.value, setSpecies)}
          ref={speciesRef}
        >
          <option value="" disabled>Choose your option</option>
          {speciesList.map((v, i) => <option key={i} value={v}>{v}</option>)}
        </select>
        <div className="invalid-feedback" style={{ display: `${species.error ? 'block' : 'none'}` }}>
          {name.error}
        </div>

        <div className={`form-group has-error ${gender.error ? 'is-invalid' : ''}`}>
          <span className="required ">Gender</span>
          <RadioOption value="male" label="Male" active={gender.value} checker={nonEmptySetter} setter={setGender} innerRef={genderRef} />
          <RadioOption value="female" label="Female" active={gender.value} checker={nonEmptySetter} setter={setGender} />
          <RadioOption value="n/a" label="n/a" active={gender.value} checker={nonEmptySetter} setter={setGender} />
          <div className="invalid-feedback" style={{ display: `${gender.error ? 'block' : 'none'}` }}>
            {gender.error}
          </div>
        </div>

        <label htmlFor="homeworld">Homeworld</label>
        <input type="text" id="homeworld" className="form-control mb-4"
          value={homeworld.value}
          onChange={e => setHomeworld({ value: e.target.value, error: '' })}
        />
        <button className="btn btn-info my-4 btn-block" disabled={submitDisabled} type="submit">Add Me!</button>
      </form>
      : <Spinner />
    }
  </div>
};