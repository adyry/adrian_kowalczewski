import React from 'react';
import { Link } from 'react-router-dom';
import HeaderLink from './HeaderLink';

const Header = (props) => {
  const [isOpen, setIsOpen] = React.useState(false);

  const toggleOpen = () => {
    setIsOpen(!isOpen);
  }

  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-primary mb-3">
      <Link to="/" className="navbar-brand">
        Sonalake Task
      </Link>

      <button
        type="button"
        onClick={toggleOpen}
        className={`navbar-toggler ${!isOpen ? 'collapsed' : ''}`}
        aria-expanded={isOpen}
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>

      <div className={`collapse navbar-collapse ${isOpen ? 'show' : ''}`}>
        <ul className="navbar-nav">
          <HeaderLink path='/' pathname={props.location.pathname} label="List View" />
          <HeaderLink path='/add' pathname={props.location.pathname} label="Add Character" />
        </ul>
      </div>
    </nav>
  )
};

export default Header;


