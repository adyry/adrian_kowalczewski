import React from 'react';
import PropTypes from 'prop-types';

const TableSearch = (props) => (
  <div className="form-group">
    <label htmlFor="searchInput" className="sr-only">Search</label>
    <input
      type="text"
      className="form-control"
      id="searchInput"
      placeholder="Search..."
      value={props.searchQuery}
      onChange={(e) => {
        const query = e.target.value;
        if (query !== props.searchQuery) props.setActivePage(1);
        props.setSearchQuery(query);
      }}
    />
  </div>
);

TableSearch.propTypes = {
  setActivePage: PropTypes.func.isRequired,
  setSearchQuery: PropTypes.func.isRequired,
  searchQuery: PropTypes.string.isRequired
}

export default TableSearch;