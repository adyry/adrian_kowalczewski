context('sonalake-task-react App', function () {
  beforeEach(() => {
    cy.visit('');
  });
  it('should display the title', () => {
    cy.get('h1').should('have.text', 'List View');
  });
  it('should render table ', () => {
    cy.get('#CharactersTable tbody');
    cy.get('#CharactersTable tbody tr');
    cy.get('#CharactersTable tbody td');
  });
  it('should return search results', () => {
    cy.get('#searchInput').type('Luke');
    cy.wait(300);
    cy.get('#CharactersTable tbody tr').should('have.length', 1);
    cy.get('#CharactersTable tbody td').eq(1).contains('Luke');
  });
  it('should navigate to add details after Add Details button click', () => {
    cy.get('#addNewButton').click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/add');
    });
  });
});
