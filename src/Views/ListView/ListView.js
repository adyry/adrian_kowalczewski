import React from 'react';
import '../../typedef'
import { Table } from '../../Shared/Table/Table';
import Pagination from '../../Shared/Pagination';
import TableTools from '../../Shared/Table/TableTools';
import { useCharacters } from '../../Helpers/hooks';

const tableConfig = {
  columns: [
    { name: "Id", value: "id" },
    { name: "Name", value: "name" },
    { name: "Species", value: "species" },
    { name: "Gender", value: "gender" },
    { name: "Homeworld", value: "homeworld" }
  ],
  showActions: true
};

export const ListView = (props) => {
  const [activePage, setActivePage] = React.useState(1);
  const [searchQuery, setSearchQuery] = React.useState("");
  const [charList, totalCount] = useCharacters(activePage, searchQuery);

  return (
    <React.Fragment>
      <h1>List View</h1>
      <TableTools
        history={props.history}
        searchQuery={searchQuery}
        setSearchQuery={setSearchQuery}
        setActivePage={setActivePage} />
      <Table
        data={charList}
        config={tableConfig}
        id="CharactersTable" />
      <Pagination
        totalCount={totalCount}
        activePage={activePage}
        setActivePage={setActivePage} />
    </React.Fragment>
  )
};