import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Header from './Shared/Header/Header';
import ListView from './Views/ListView';
import AddCharacter from './Views/AddCharacter';
import ErrorBoundary from './Helpers/ErrorBoundary';

const App = () => {
  return (
    <div className="App">
      <Router>
        <Route path="/" render={props => <Header {...props} />} />
        <div className="container">
          <ErrorBoundary>
            <Route exact path="/" component={ListView} />
            <Route path="/add" component={AddCharacter} />
          </ErrorBoundary>
        </div>

      </Router>
    </div>
  );
};

export default App