import React from 'react';

class ErrorBoundary extends React.Component {
  state = { hasError: false };

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    console.log(error, info);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <span>Sorry, something went wrong with Application. Please try to refresh page, or, if it won't help, <a href="mailto:adrian.kowalczewski@gmail.com">contact the administrator</a></span>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;