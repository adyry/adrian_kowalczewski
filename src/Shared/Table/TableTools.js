import React from 'react';
import TableSearch from './TableSearch';

const TableTools = (props) => (
  <div className="row">
    <div className="col-sm-6">
      <TableSearch
        setActivePage={props.setActivePage}
        setSearchQuery={props.setSearchQuery}
        searchQuery={props.searchQuery}
      />
    </div>
    <div className="col-sm-6 text-sm-right">
      <button type="button" className="btn btn-primary mb-3" id="addNewButton" onClick={() => props.history.push('/add')}>
        Add New
    </button>
    </div>
  </div>
);

export default TableTools;