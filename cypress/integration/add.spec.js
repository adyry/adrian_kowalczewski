describe('Add Character', function () {
  beforeEach(() => {
    cy.visit('/add');
  });
  it('should display the title', () => {
    cy.get('.h4').should('have.text', 'Add Character');
  });
  // test below is outdated (should be adjusted to radio buttons and to navigate to last page)
  // it('should Add Character and navigate to List after proper form filling', () => {
  //   cy.get('#firstName').type('Test Character');
  //   cy.get('#SpeciesSelect').select('Dug');
  //   cy.get('#GenderSelect').select('female');
  //   cy.get('#homeworld').type('Kashyyk');
  //   cy.get('button[type="submit"]').click();
  //   cy.location().should((loc) => {
  //     expect(loc.pathname).to.eq('/');
  //   });
  //   cy.wait(1500);
  //   cy.get('#CharactersTable tbody tr:last-child td', { timeout: 3000 }).as('row')
  //   cy.get('@row').eq(0).contains('Test Character');
  //   cy.get('@row').eq(1).contains('Dug');
  //   cy.get('@row').eq(2).contains('female');
  //   cy.get('@row').eq(3).contains('Kashyyk');
  // });
});