/**
 * @typedef {Object} Character
 * @property {string} gender
 * @property {string} homeworld
 * @property {number} id
 * @property {string} name
 * @property {string} species
 */
