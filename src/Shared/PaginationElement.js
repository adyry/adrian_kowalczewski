import React from 'react';
import PropTypes from 'prop-types';

const PaginationElement = React.memo((props) => (
  <li className={`page-item ${props.disabled ? 'disabled' : ''} ${props.active ? 'active' : ''}`}>
    <button onClick={props.clickHandler}
      type="button" className="page-link" tabIndex={props.disabled ? "-1" : "0"}>
      {props.label} {props.active ? <span className="sr-only">(current)</span> : null}
    </button>
  </li>
));

PaginationElement.propTypes = {
  disabled: PropTypes.bool,
  clickHandler: PropTypes.func,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number]),
  active: PropTypes.bool
}
PaginationElement.defaultProps = {
  active: false
}

export default PaginationElement;