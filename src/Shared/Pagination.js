import React from 'react';
import PaginationElement from './PaginationElement';

/**
 * 
 * @param {{totalCount: string, activePage: string, setActivePage: Function}} props 
 */
const Pagination = (props) => {
  const pageSize = process.env.REACT_APP_POST_PER_PAGE;
  const totalPages = Math.ceil(props.totalCount / pageSize);

  const generatePages = () => {
    const elements = [];
    for (let i = 1; i <= totalPages; i++) {
      elements.push(
        <PaginationElement key={i}
          active={props.activePage === i}
          clickHandler={() => { props.setActivePage(i) }}
          label={i}
        />)
    }
    return elements;
  }

  const isFirst = props.activePage === 1;
  const isLast = (props.activePage === totalPages) || totalPages === 0;

  return <nav aria-label="Data grid navigation">
    <ul className="pagination justify-content-end">
      <PaginationElement disabled={isFirst} clickHandler={() => { props.setActivePage(1) }} label="First" />
      <PaginationElement disabled={isFirst} clickHandler={() => { props.setActivePage(props.activePage - 1) }} label="Previous" />
      {generatePages()}
      <PaginationElement disabled={isLast} clickHandler={() => { props.setActivePage(props.activePage + 1) }} label="Next" />
      <PaginationElement disabled={isLast} clickHandler={() => { props.setActivePage(totalPages) }} label="Last" />
    </ul>
  </nav>

}
export default Pagination;