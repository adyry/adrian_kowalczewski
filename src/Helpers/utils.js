export const uid = () => Math.ceil(Math.random() * 10000);

export const baseUrl = "http://localhost:3000"

export const fetchUtil = (url, method = 'POST', body) => {
  let options;
  if (body) {
    options = {
      method,
      headers: { 'Content-Type': 'application/json' },
      body
    }
  };

  return fetch(`${baseUrl}${url}`, options)
    .then((response) => response.json());
}