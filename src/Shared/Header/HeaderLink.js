import React from 'react';
import { Link } from 'react-router-dom';

const HeaderLink = React.memo((props) => <li className={`nav-item ${props.pathname === props.path ? 'active' : ''}`}>
  <Link to={props.path} className="nav-link">
    {props.label} {props.pathname === props.path ? <span className="sr-only">(current)</span> : null}
  </Link>
</li>);

export default HeaderLink;