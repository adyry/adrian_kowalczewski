import { useState, useEffect } from 'react';
import '../typedef'
import { baseUrl, fetchUtil } from './utils';

/**
 * @param {any} value 
 * @param {number} delay 
 */
const useDebounce = (value, delay) => {
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]
  );
  return debouncedValue;
}

/**
 * @param {Number} page 
 * @param {string} searchQuery
 * @return {Character[]} 
 */
export const useCharacters = (page, searchQuery) => {
  const [list, setList] = useState(null);
  const [count, setCount] = useState(null);
  const debouncedSearchTerm = useDebounce(searchQuery, 200);

  useEffect(() => {
    // cant use util due Header info 
    fetch(`${baseUrl}/characters?_page=${page}&_limit=${process.env.REACT_APP_POST_PER_PAGE}&q=${debouncedSearchTerm}`)
      .then(response => {
        setCount(response.headers.get('X-Total-Count'));
        return response.json()
      })
      .then(response => {
        setList(response);
      });
  }, [page, debouncedSearchTerm]);

  return [list, count];
}

/** 
 * @return {string[]} 
 */
export const useSpecies = () => {
  const [species, setSpecies] = useState([]);
  useEffect(() => {
    fetchUtil('/species')
      .then(response => {
        setSpecies(response)
      });
  }, []);

  return species;
}

