import React from 'react';
import { uid } from '../../Helpers/utils'

const RadioOption = React.memo((props) => {
  const uniq = uid();
  return <div className="form-check">
    <input id={uniq} type="radio" className="form-check-input"
      value={props.value}
      checked={props.active === props.value}
      onChange={(e) => props.checker(e.target.value, props.setter)}
      ref={(props.innerRef) ? props.innerRef : null} />
    <label htmlFor={uniq} className="form-check-label">{props.label}</label>
  </div>
});

export default RadioOption;