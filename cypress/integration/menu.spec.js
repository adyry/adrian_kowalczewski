describe('Menu', function () {
  beforeEach(() => {
    cy.visit('');
    cy.get('.nav-item').as('menu');
  });
  it('should have menu with two items', function () {
    cy.get('@menu').should('have.length', 2);
    cy.get('@menu').first().contains('List View')
    cy.get('@menu').last().contains('Add Character');
  });
  it('should navigate to add details after menu click', () => {
    cy.get('@menu').eq(1).click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/add');
    });
  });
  it('should navigate to list after menu click on /add route', () => {
    cy.visit('/add');
    cy.get('@menu').eq(0).click();
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/');
    });
  });
});
